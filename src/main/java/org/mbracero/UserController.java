package org.mbracero;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@Autowired
    private UserRepository userRepository;
	
	@RequestMapping(path = "/users",method = RequestMethod.GET)
    public List<UserDTO> getUsers() {
        return userRepository.getUsers();
    }
	
    @RequestMapping(path = "/users/first",method = RequestMethod.GET)
    public UserDTO getFirstUser() {
        return userRepository.getFirstUser();
    }
    
    @RequestMapping(path = "/users/last",method = RequestMethod.GET)
    public UserDTO getLastUser() {
        return userRepository.getLastUser();
    }
    
    @RequestMapping(path = "/users/{company}",method = RequestMethod.GET)
    public UserDTO getUserByCompany(final @PathVariable("company") String company) {
        return userRepository.getUserByCompany(company);
    }
        
    @RequestMapping(path = "/users/user/{eyecolor}/{email}",method = RequestMethod.GET)
    public UserDTO getUserByUserEmailAndEyeColor(final @PathVariable("eyecolor") String eyeColor,
    		final @PathVariable("email") String email) {
    	final UserDTO user = new UserDTO("", "", 0, eyeColor, "", email);
        return userRepository.getUserByUserEmailAndEyeColor(user);
    }
    
    @RequestMapping(path = "/users/user/{email}",method = RequestMethod.GET)
    public UserDTO getUserByUserEmail(final @PathVariable("email") String email) {
    	final UserDTO user = new UserDTO("", "", 0, "", "", email);
        return userRepository.getUserByUserEmail(user);
    }
}
