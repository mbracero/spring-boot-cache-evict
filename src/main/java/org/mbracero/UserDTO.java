package org.mbracero;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UserDTO {

	private String id = "";
	private String name = "";
	private int age = 0;
	private String eyeColor= "";
	private String company = "";
	private String email = "";
	
	public UserDTO() {
		super();
	}
	
	public UserDTO(String id, String name, int age, String eyeColor,
			String company, String email) {
		super();
		this.id = id;
		this.name = name;
		this.age = age;
		this.eyeColor = eyeColor;
		this.company = company;
		this.email = email;
	}
	
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	
	@JsonProperty("_id")
	public void setId(String id) {
		this.id = id;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int getAge() {
		return age;
	}
	
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getEyeColor() {
		return eyeColor;
	}
	
	public void setEyeColor(String eyeColor) {
		this.eyeColor = eyeColor;
	}
	
	public String getCompany() {
		return company;
	}
	
	public void setCompany(String company) {
		this.company = company;
	}
	
	public String getEmail() {
		return email;
	}
	
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "UserDTO [id=" + id + ", name=" + name + ", age=" + age
				+ ", eyeColor=" + eyeColor + ", company=" + company
				+ ", email=" + email + "]";
	}
	
	
}
