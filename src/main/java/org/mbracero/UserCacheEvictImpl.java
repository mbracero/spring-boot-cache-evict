package org.mbracero;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Component;

@Component
public class UserCacheEvictImpl implements UserCacheEvict {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserCacheEvictImpl.class);
	
	@CacheEvict(cacheNames="users", allEntries=true)
	public void evictUsersCache() {
		LOGGER.info("################### clean cache users");
	}
	
	@CacheEvict(cacheNames="userBy", allEntries=true)
	public void evictUserByCache() {
		LOGGER.info("################### clean cache userBy");
	}
}
