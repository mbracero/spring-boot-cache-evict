package org.mbracero;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.ehcache.EhCacheCacheManager;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;

@SpringBootApplication
@EnableCaching
public class SpringBootCacheEvictApplication {
	
	@Value("classpath:/conf/cache-conf.xml")
	private Resource cacheResource;
	
	@Bean
	public EhCacheManagerFactoryBean getEhCacheFactory(){
		EhCacheManagerFactoryBean factoryBean = new EhCacheManagerFactoryBean();
		factoryBean.setConfigLocation(cacheResource);
		factoryBean.setShared(true);
		return factoryBean;
	}
	
	/**
	 * Damos preferencia a este bean bajo multiples candidatos
	 */
	@Bean
	@Primary
	public CacheManager getEhCacheManager(){
	        return new EhCacheCacheManager(getEhCacheFactory().getObject());
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCacheEvictApplication.class, args);
	}
}
