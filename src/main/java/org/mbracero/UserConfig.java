package org.mbracero;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
public class UserConfig {
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Value("classpath:/static/users.json")
	private Resource userResource;
	
	@Bean
	public List<UserDTO> users() throws IOException {
		InputStream is = userResource.getInputStream();
		UserDTO[] users = objectMapper.readValue(is, UserDTO[].class);
		
		return Collections.unmodifiableList( Arrays.asList(users) );
	}
}
