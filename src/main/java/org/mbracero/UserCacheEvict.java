package org.mbracero;

public interface UserCacheEvict {
	public void evictUsersCache();
	public void evictUserByCache();
}
