package org.mbracero;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CacheController {
	
	@Autowired
	UserCacheEvict userCacheEvict;
	
	@RequestMapping(path = "/cache/clear/{name}", method = RequestMethod.GET)
    public String clearCache(final @PathVariable("name") String name) {
		String ret = "KO";
		
		if("users".equalsIgnoreCase(name)) {
			userCacheEvict.evictUsersCache();
			ret = "OK";
		} else if("userBy".equalsIgnoreCase(name)) {
			userCacheEvict.evictUserByCache();
			ret = "OK";
		}
		
        return ret;
    }
	
}
