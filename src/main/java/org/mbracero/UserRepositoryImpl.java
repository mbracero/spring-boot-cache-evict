package org.mbracero;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Repository;

@Repository
public class UserRepositoryImpl implements UserRepository {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UserRepositoryImpl.class);
	
	@Autowired
	private List<UserDTO> users;

	@Override
	@Cacheable("users")
	public List<UserDTO> getUsers() {
		LOGGER.info("============================ Get all users");
		return users;
	}

	@Override
	@Cacheable(cacheNames="userBy", key = "#root.methodName")
	public UserDTO getFirstUser() {
		LOGGER.info("============================ Get first user");
		return users.get(0);
	}
	
	@Override
	public UserDTO getLastUser() {
		LOGGER.info("============================ Get last user");
		return users.get(users.size()-1);
	}

	@Override
	@Cacheable(cacheNames="userBy", key = "{#company}")
	public UserDTO getUserByCompany(String company) {
		LOGGER.info("============================ Get user by company {}", company);
		return users.stream()
				.filter(u -> company.equalsIgnoreCase(u.getCompany()))
				.findFirst().orElse(null);
	}

	@Override
	@Cacheable(cacheNames="userBy", key = "#user.email")
	public UserDTO getUserByUserEmail(UserDTO user) {
		String email = user.getEmail();
		LOGGER.info("============================ Get user by email {}", email);
		return users.stream()
				.filter(u -> u.getEmail().contains(email))
				.findFirst().orElse(null);
	}
	
	@Override
	@Cacheable(cacheNames="userBy", key = "{#user.email, #user.eyeColor}")
	public UserDTO getUserByUserEmailAndEyeColor(UserDTO user) {
		String email = user.getEmail();
		String eyeColor = user.getEyeColor();
		LOGGER.info("============================ Get user by email and eyeColor {} - {}", email, eyeColor);
		return users.stream()
				.filter(u -> u.getEmail().contains(email) && eyeColor.equalsIgnoreCase(u.getEyeColor()))
				.findFirst().orElse(null);
	}
}
