package org.mbracero;

import java.util.List;

public interface UserRepository {
	List<UserDTO> getUsers();
	UserDTO getFirstUser();
	UserDTO getLastUser();
	UserDTO getUserByCompany(String company);
	UserDTO getUserByUserEmail(UserDTO user);
	UserDTO getUserByUserEmailAndEyeColor(UserDTO user);
}
